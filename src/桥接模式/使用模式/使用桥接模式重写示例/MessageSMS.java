﻿package 桥接模式.使用模式.使用桥接模式重写示例;
/**
 * 以站内短消息的方式发送消息
 */
public  class MessageSMS implements MessageImplementor{

	public void send(String message, String toUser) {
		System.out.println("使用站内短消息的方式，发送消息'"+message+"'给"+toUser);
	}
}
