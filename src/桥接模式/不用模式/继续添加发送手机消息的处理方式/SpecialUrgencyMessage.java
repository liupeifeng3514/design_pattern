﻿package 桥接模式.不用模式.继续添加发送手机消息的处理方式;

/**
 * 特急消息的抽象接口
 */
public interface SpecialUrgencyMessage extends Message {
	public void hurry(String messageId);
}
