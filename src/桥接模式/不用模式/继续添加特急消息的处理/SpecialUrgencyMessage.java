﻿package 桥接模式.不用模式.继续添加特急消息的处理;

/**
 * 特急消息的抽象接口
 */
public interface SpecialUrgencyMessage extends Message {
	public void hurry(String messageId);
}
