﻿package 生成器模式.生成器模式示例代码;

/**
 * 被构建的产品对象的接口
 */
public interface Product {
	// 定义产品的操作
}